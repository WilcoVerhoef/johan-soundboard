module.exports = {
  "globDirectory": "public/",
  "globPatterns": [ "**" ],
  "importScripts": [ "assets/reload-on-update.js" ],
  "swDest": "public/service-worker.js",
  "clientsClaim": true,
  "skipWaiting": true,
  "cleanupOutdatedCaches": true,
  "inlineWorkboxRuntime": true,
  "sourcemap": false,
  // "mode": "debug",
};