self.addEventListener('activate', event => {
  event.waitUntil(
    clients.matchAll()
      .then(clients => clients.forEach(
        client => client.postMessage('reload')
      ))
  );
});
